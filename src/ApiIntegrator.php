<?php

namespace Davek1312\ApiIntegrator;

use Davek1312\ApiIntegrator\Models\ApiIntegratorErrorModel;
use Davek1312\ApiIntegrator\Models\ApiIntegratorRequestModel;
use Davek1312\ApiIntegrator\Models\ApiIntegratorResponseModel;
use GuzzleHttp\Exception\TransferException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\TransferStats;

/**
 * Retrieve's data from a third party API and deserialises the response into a model object
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
abstract class ApiIntegrator {

    /**
     * @var string
     */
    public static $responseModelClass;

    /**
     * @var ApiIntegratorRequestModel
     */
    protected $requestModel;
    /**
     * @var ApiIntegratorResponseModel
     */
    protected $responseModel;
    /**
     * @var Response
     *
     * @Exclude()
     */
    protected $httpResponse;
    /**
     * RequestException thrown in generateHttpResponse()
     *
     * @var TransferException
     */
    protected $transferException;
    /**
     * @var double
     */
    protected $requestTimeInSeconds;
    /**
     * @var integer
     */
    protected $httpStatusCode;
    /**
     * The response body contents from the HTTP request
     *
     * @var string
     */
    protected $responseBodyContents;
    /**
     * @var integer
     */
    protected $countRequestAttempts = 0;

    /**
     * ApiIntegrator constructor. Sets $httpResponse from $requestModel.
     *
     * @param ApiIntegratorRequestModel $requestModel
     */
    public function __construct(ApiIntegratorRequestModel $requestModel) {
        $this->requestModel = $requestModel;
    }

    /**
     * Deserialises the responseBody and sets and return $responseModel
     *
     * @return ApiIntegratorResponseModel
     */
    public function generateResponseModel() {
        $responseModelClass = static::$responseModelClass;
        $this->responseModel = $responseModelClass::deserialiseResponse($this->responseBodyContents);
        return $this->responseModel;
    }

    /**
     * Calls generateNewHttpResponse() and generateResponseModel()
     *
     * @param integer $maxRequestAttempts
     *
     * @return ApiIntegratorResponseModel
     */
    public function generateNewResponseModel($maxRequestAttempts = 1) {
        while($this->attemptRequest($maxRequestAttempts)) {
            $this->generateNewHttpResponse();
            $this->generateResponseModel();
            $this->countRequestAttempts++;
        }
        return $this->responseModel;
    }

    /**
     * @param integer $maxRequestAttempts
     *
     * @return boolean
     */
    protected function attemptRequest($maxRequestAttempts) {
        return $this->countRequestAttempts == 0 || ($this->countRequestAttempts < $maxRequestAttempts && $this->getErrorModel()->hasTransferError());
    }

    /**
     * Makes the HTTP request to the API and set $httpResponse property and transfer stats properties
     *
     * @return Response|null
     */
    public function generateNewHttpResponse() {
        try {
            $httpResponse = $this->requestModel->getHttpClient()->request($this->requestModel->getConnectionMethod(), $this->requestModel->getUrl(),  [
                'on_stats' => function(TransferStats $transferStats) {
                    $this->processTransferStats($transferStats);
                }
            ]);
            $this->processHttpResponse($httpResponse);
        }
        catch(TransferException $e) {
            $this->transferException = $e;
        }
        return $this->httpResponse;
    }

    /**
     * Sets object properties from the response TransferStats object.
     *
     * @param TransferStats $transferStats
     */
    private function processTransferStats(TransferStats $transferStats) {
        $this->requestTimeInSeconds = $transferStats->getTransferTime();
        if($transferStats->hasResponse()) {
            $response = $transferStats->getResponse();
            $this->httpStatusCode = $response->getStatusCode();
        }
    }

    /**$
     * Sets object properties from the response httpResponse object.
     *
     * @param Response $httpResponse
     */
    private function processHttpResponse(Response $httpResponse) {
        $this->httpResponse = $httpResponse;
        $body = $this->httpResponse->getBody();
        if($body) {
            $this->responseBodyContents = $body->getContents();
        }
    }

    /**
     * @return ApiIntegratorRequestModel
     */
    public function getRequestModel() {
        return $this->requestModel;
    }

    /**
     * @param ApiIntegratorRequestModel $requestModel
     */
    public function setRequestModel($requestModel) {
        $this->requestModel = $requestModel;
    }

    /**
     * @return ApiIntegratorResponseModel
     */
    public function getResponseModel() {
        return $this->responseModel;
    }

    /**
     * @param ApiIntegratorResponseModel $responseModel
     */
    public function setResponseModel($responseModel) {
        $this->responseModel = $responseModel;
    }

    /**
     * @return Response
     */
    public function getHttpResponse() {
        return $this->httpResponse;
    }

    /**
     * @param Response $httpResponse
     */
    public function setHttpResponse($httpResponse) {
        $this->httpResponse = $httpResponse;
    }

    /**
     * @return TransferException
     */
    public function getTransferException() {
        return $this->transferException;
    }

    /**
     * @param TransferException $transferException
     */
    public function setTransferException($transferException) {
        $this->transferException = $transferException;
    }

    /**
     * @return double
     */
    public function getRequestTimeInSeconds() {
        return $this->requestTimeInSeconds;
    }

    /**
     * @param double $requestTimeInSeconds
     */
    public function setRequestTimeInSeconds($requestTimeInSeconds) {
        $this->requestTimeInSeconds = $requestTimeInSeconds;
    }

    /**
     * @return int
     */
    public function getHttpStatusCode() {
        return $this->httpStatusCode;
    }

    /**
     * @param int $httpStatusCode
     */
    public function setHttpStatusCode($httpStatusCode) {
        $this->httpStatusCode = $httpStatusCode;
    }

    /**
     * @return string
     */
    public function getResponseBodyContents() {
        return $this->responseBodyContents;
    }

    /**
     * @param string $responseBodyContents
     */
    public function setResponseBodyContents($responseBodyContents) {
        $this->responseBodyContents = $responseBodyContents;
    }

    /**
     * @return boolean
     */
    public function hasTransferError() {
        return $this->transferException != null;
    }

    /**
     * @return boolean
     */
    public function hasErrors() {
        return $this->getErrorModel()->hasErrors();
    }

    /**
     * @return ApiIntegratorErrorModel
     */
    public function getErrorModel() {
        return new ApiIntegratorErrorModel($this);
    }

    /**
     * @return int
     */
    public function getCountRequestAttempts() {
        return $this->countRequestAttempts;
    }

    /**
     * @param int $countRequestAttempts
     */
    public function setCountRequestAttempts($countRequestAttempts) {
        $this->countRequestAttempts = $countRequestAttempts;
    }
}
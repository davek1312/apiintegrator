<?php

namespace Davek1312\ApiIntegrator\Models;

/**
 * @author  David Kelly <davek1312@gmail.com>
 */
interface ApiIntegratorResponseModelInterface {

    /**
     * The API's response format: 'json' OR 'xml'
     *
     * @return  string
     */
    public static function getResponseDataType();

    /**
     * Returns the error message attribute returned by the API.
     *
     * @return string|null
     */
    public function getResponseErrorMessage();

    /**
     * Returns the error code attribute returned by the API.
     *
     * @return string|null
     */
    public function getResponseErrorCode();
}
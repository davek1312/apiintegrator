<?php

namespace Davek1312\ApiIntegrator\Models;

use Davek1312\ApiIntegrator\ApiIntegrator;
use Davek1312\Serialise\Traits\Common;

/**
 * Common model for any possible error's that occurred creating a response model from a HTTP response
 *
 * @author David Kelly <davek1312@gmail.com>
 */
class ApiIntegratorErrorModel {

    use Common;

    /**
     * @var ApiIntegrator
     */
    private $apiIntegrator;
    /**
     * @var string
     */
    private $transferExceptionMessage;
    /**
     * @var string
     */
    private $transferExceptionCode;
    /**
     * @var string
     */
    private $responseModelErrorMessage;
    /**
     * @var string
     */
    private $responseModelErrorCode;
    /**
     * @var string
     */
    private $deserialiseExceptionMessage;
    /**
     * @var string
     */
    private $deserialiseExceptionCode;

    /**
     * ApiIntegratorErrorModel constructor.
     *
     * @param ApiIntegrator $apiIntegrator
     */
    public function __construct(ApiIntegrator $apiIntegrator = null) {
        $this->apiIntegrator = $apiIntegrator;
    }

    /**
     * @return boolean
     */
    public function hasTransferError() {
        return $this->getTransferExceptionMessage() != null;
    }

    /**
     * @return string
     */
    public function getTransferExceptionMessage() {
        if(!$this->transferExceptionMessage) {
            $this->transferExceptionMessage = $this->getSubelement([
                'getApiIntegrator',
                'getTransferException',
                'getMessage'
            ]);
        }
        return $this->transferExceptionMessage;
    }

    /**
     * @param string $transferExceptionMessage
     */
    public function setTransferExceptionMessage($transferExceptionMessage) {
        $this->transferExceptionMessage = $transferExceptionMessage;
    }

    /**
     * @return string
     */
    public function getTransferExceptionCode() {
        if(!$this->transferExceptionCode) {
            $this->transferExceptionCode = $this->getSubelement([
                'getApiIntegrator',
                'getTransferException',
                'getCode'
            ]);
        }
        return $this->transferExceptionCode;
    }

    /**
     * @param string $transferExceptionCode
     */
    public function setTransferExceptionCode($transferExceptionCode) {
        $this->transferExceptionCode = $transferExceptionCode;
    }

    /**
     * @return boolean
     */
    public function hasResponseError() {
        return $this->getResponseModelErrorMessage() != null || $this->getResponseModelErrorCode() != null;
    }

    /**
     * @return string
     */
    public function getResponseModelErrorMessage() {
        if(!$this->responseModelErrorMessage) {
            $this->responseModelErrorMessage = $this->getSubelement([
                'getApiIntegrator',
                'getResponseModel',
                'getResponseErrorMessage'
            ]);
        }
        return $this->responseModelErrorMessage;
    }

    /**
     * @param string $responseModelErrorMessage
     */
    public function setResponseModelErrorMessage($responseModelErrorMessage) {
        $this->responseModelErrorMessage = $responseModelErrorMessage;
    }

    /**
     * @return string
     */
    public function getResponseModelErrorCode() {
        if(!$this->responseModelErrorCode) {
            $this->responseModelErrorCode = $this->getSubelement([
                'getApiIntegrator',
                'getResponseModel',
                'getResponseErrorCode'
            ]);
        }
        return $this->responseModelErrorCode;
    }

    /**
     * @param string $responseModelErrorCode
     */
    public function setResponseModelErrorCode($responseModelErrorCode) {
        $this->responseModelErrorCode = $responseModelErrorCode;
    }

    /**
     * @return boolean
     */
    public function hasDeserialiseError() {
        return $this->getDeserialiseExceptionMessage() !== null;
    }

    /**
     * @return string
     */
    public function getDeserialiseExceptionMessage() {
        if(!$this->deserialiseExceptionMessage) {
            $this->deserialiseExceptionMessage = $this->getSubelement([
                'getApiIntegrator',
                'getResponseModel',
                'getDeserialiseException',
                'getMessage'
            ]);
        }
        return $this->deserialiseExceptionMessage;
    }

    /**
     * @param string $deserialiseExceptionMessage
     */
    public function setDeserialiseExceptionMessage($deserialiseExceptionMessage) {
        $this->deserialiseExceptionMessage = $deserialiseExceptionMessage;
    }

    /**
     * @return string
     */
    public function getDeserialiseExceptionCode() {
        if(!$this->deserialiseExceptionCode) {
            $this->deserialiseExceptionCode = $this->getSubelement([
                'getApiIntegrator',
                'getResponseModel',
                'getDeserialiseException',
                'getCode'
            ]);
        }
        return $this->deserialiseExceptionCode;
    }

    /**
     * @param string $deserialiseExceptionCode
     */
    public function setDeserialiseExceptionCode($deserialiseExceptionCode) {
        $this->deserialiseExceptionCode = $deserialiseExceptionCode;
    }

    /**
     * @return boolean
     */
    public function hasErrors() {
        return $this->hasTransferError() || $this->hasResponseError() || $this->hasDeserialiseError();
    }

    /**
     * @return ApiIntegrator
     */
    public function getApiIntegrator() {
        return $this->apiIntegrator;
    }

    /**
     * @param ApiIntegrator $apiIntegrator
     */
    public function setApiIntegrator($apiIntegrator) {
        $this->apiIntegrator = $apiIntegrator;
    }
}
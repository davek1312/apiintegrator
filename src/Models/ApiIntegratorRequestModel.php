<?php

namespace Davek1312\ApiIntegrator\Models;

use GuzzleHttp\Client;

/**
 * Model for api requests
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
class ApiIntegratorRequestModel {

    /**
     * The API's url
     *
     * @var string
     */
    protected $url;
    /**
     * HTTP connection method eg. GET, POST, PUT..
     *
     * @var string
     */
    protected $connectionMethod;
    /**
     * Number of seconds to wait while trying to connect to the API
     *
     * @var double
     */
    protected $connectTimeout;
    /**
     * Timeout of the request in seconds
     *
     * @var double
     */
    protected $requestTimeout;
    /**
     * Build your request using this documentation: http://docs.guzzlephp.org/en/latest/request-options.html
     *
     * @var array
     */
    protected $additionalRequestOptions;

    /**
     * ApiIntegratorRequestModel constructor.
     *
     * @param string $url
     * @param string $connectionMethod
     * @param double $connectTimeout
     * @param double $requestTimeout
     * @param array $additionalRequestOptions
     */
    public function __construct($url, $connectionMethod, $connectTimeout, $requestTimeout, array $additionalRequestOptions = []) {
        $this->url = $url;
        $this->connectionMethod = $connectionMethod;
        $this->connectTimeout = $connectTimeout;
        $this->requestTimeout = $requestTimeout;
        $this->additionalRequestOptions = $additionalRequestOptions;
    }

    /**
     * Returns a new GuzzleHttp\Client with all the request options
     *
     * @return Client
     */
    public function getHttpClient() {
        return new Client($this->getAllRequestOptions());
    }

    /**
     * Merges the default request options with the additional options
     *
     * @return array
     */
    public function getAllRequestOptions() {
        return array_merge($this->additionalRequestOptions, $this->getDefaultRequestOptions());
    }

    /**
     * Creates an array of default request options from object properties
     *
     * @return array
     */
    public function getDefaultRequestOptions() {
        return [
            'connect_timeout' => $this->connectTimeout,
            'timeout' => $this->requestTimeout,
        ];
    }

    /**
     * @return string
     */
    public function getUrl() {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url) {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getConnectionMethod() {
        return $this->connectionMethod;
    }

    /**
     * @param string $connectionMethod
     */
    public function setConnectionMethod($connectionMethod) {
        $this->connectionMethod = $connectionMethod;
    }

    /**
     * @return double
     */
    public function getConnectTimeout() {
        return $this->connectTimeout;
    }

    /**
     * @param double $connectTimeout
     */
    public function setConnectTimeout($connectTimeout) {
        $this->connectTimeout = $connectTimeout;
    }

    /**
     * @return double
     */
    public function getRequestTimeout() {
        return $this->requestTimeout;
    }

    /**
     * @param double $requestTimeout
     */
    public function setRequestTimeout($requestTimeout) {
        $this->requestTimeout = $requestTimeout;
    }

    /**
     * @return array
     */
    public function getAdditionalRequestOptions() {
        return $this->additionalRequestOptions;
    }

    /**
     * @param array $additionalRequestOptions
     */
    public function setAdditionalRequestOptions($additionalRequestOptions) {
        $this->additionalRequestOptions = $additionalRequestOptions;
    }
}
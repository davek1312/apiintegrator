<?php

namespace Davek1312\ApiIntegrator\Models;

use Davek1312\Serialise\Models\BaseSerialiseModel;
use Davek1312\VariableUtils\StringUtils;

/**
 * Model for api responses
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
abstract class ApiIntegratorResponseModel extends BaseSerialiseModel implements ApiIntegratorResponseModelInterface {

    /**
     * Deserialises $data into an object of this class
     *
     * @param string $data
     *
     * @return object
     */
    public static function deserialiseResponse($data) {
        return static::deserialise($data, static::getResponseDataType());
    }

    /**
     * @return boolean
     */
    public function hasResponseError() {
        return !StringUtils::isEmptyOrNull($this->getResponseErrorMessage()) || !StringUtils::isEmptyOrNull($this->getResponseErrorCode());
    }
}
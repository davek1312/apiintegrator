<?php

namespace Davek1312\ApiIntegrator\Tests\Models;

use Davek1312\ApiIntegrator\Tests\Mock\Models\ResponseModel;

class ApiIntegratorResponseModelTest extends \PHPUnit_Framework_TestCase {

    private $responseModel;

    public function setUp() {
        parent::setUp();
        $this->responseModel = new ResponseModel();
    }

    public function testGetResponseErrorMessage() {
        $this->assertNull($this->responseModel->getResponseErrorMessage());
        $this->responseModel->setError(true);
        $this->assertSame(true, $this->responseModel->getResponseErrorMessage());
    }

    public function testGetResponseErrorCode() {
        $this->assertNull($this->responseModel->getResponseErrorCode());
        $this->responseModel->setErrorCode(true);
        $this->assertSame(true, $this->responseModel->getResponseErrorCode());
    }

    public function testHasResponseError() {
        $this->assertFalse($this->responseModel->hasResponseError());
        $this->responseModel->setError(true);
        $this->assertTrue($this->responseModel->hasResponseError());
        $this->responseModel->setErrorCode(true);
        $this->assertTrue($this->responseModel->hasResponseError());
        $this->responseModel->setError(null);
        $this->assertTrue($this->responseModel->hasResponseError());
    }

    public function testDeserialiseResponse() {
        $responseModel = ResponseModel::getResponseModelFromJson();
        $this->assertSame(1, $responseModel->getPostId());
        $this->assertSame(1, $responseModel->getId());
        $this->assertSame('id labore ex et quam laborum', $responseModel->getName());
        $this->assertSame('Eliseo@gardner.biz', $responseModel->getEmail());
        $this->assertSame("laudantium enim quasi est quidem magnam voluptate ipsam eos\ntempora quo necessitatibus\ndolor quam autem quasi\nreiciendis et nam sapiente accusantium", $responseModel->getBody());
    }
}
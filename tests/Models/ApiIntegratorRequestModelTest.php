<?php

namespace Davek1312\ApiIntegrator\Tests\Models;

use Davek1312\ApiIntegrator\Models\ApiIntegratorRequestModel;
use Davek1312\ApiIntegrator\Tests\Mock\Models\RequestModel;

class ApiIntegratorRequestModelTest extends \PHPUnit_Framework_TestCase {

    const DEFAULT_REQUEST_OPTIONS = [
        'connect_timeout' => RequestModel::CONNECTION_TIMEOUT,
        'timeout' => RequestModel::REQUEST_TIMEOUT,
    ];

    public function test__construct() {
        $requestModel = RequestModel::constructNewRequestModel();
        $this->assertRequestModelEqualsConstantValues($requestModel);
    }

    private function assertRequestModelEqualsConstantValues(ApiIntegratorRequestModel $requestModel) {
        $this->assertSame(RequestModel::URL, $requestModel->getUrl());
        $this->assertSame(RequestModel::CONNECTION_METHOD, $requestModel->getConnectionMethod());
        $this->assertSame(RequestModel::CONNECTION_TIMEOUT, $requestModel->getConnectTimeout());
        $this->assertSame(RequestModel::REQUEST_TIMEOUT, $requestModel->getRequestTimeout());
        $this->assertSame(RequestModel::ADDITIONAL_REQUEST_OPTIONS, $requestModel->getAdditionalRequestOptions());
    }

    public function testGetHttpClient() {
        $requestModel = RequestModel::constructNewRequestModel();
        $httpClient = $requestModel->getHttpClient();
        $this->assertInstanceOf('GuzzleHttp\Client', $httpClient);
        $this->assertSame(RequestModel::REQUEST_TIMEOUT, $httpClient->getConfig('timeout'));
        $this->assertSame(RequestModel::CONNECTION_TIMEOUT, $httpClient->getConfig('connect_timeout'));
        $this->assertSame(RequestModel::ADDITIONAL_REQUEST_OPTIONS['debug'], $httpClient->getConfig('debug'));
        $this->assertSame(RequestModel::ADDITIONAL_REQUEST_OPTIONS['verify'], $httpClient->getConfig('verify'));
    }

    public function testGetAllRequestOptions() {
        $requestModel = RequestModel::constructNewRequestModel();
        $allRequestOptions = array_merge(RequestModel::ADDITIONAL_REQUEST_OPTIONS, static::DEFAULT_REQUEST_OPTIONS);
        $this->assertSame($allRequestOptions, $requestModel->getAllRequestOptions());
    }

    public function testGetDefaultRequestOptions() {
        $requestModel = RequestModel::constructNewRequestModel();
        $this->assertSame(static::DEFAULT_REQUEST_OPTIONS, $requestModel->getDefaultRequestOptions());
    }
}
<?php

namespace Davek1312\ApiIntegrator\Tests\Models;

use Davek1312\ApiIntegrator\Models\ApiIntegratorErrorModel;
use Davek1312\ApiIntegrator\Tests\Mock\Integrator;
use Davek1312\ApiIntegrator\Tests\Mock\Models\ResponseModel;
use GuzzleHttp\Exception\TransferException;
use Exception;

class ApiIntegratorErrorModelTest extends \PHPUnit_Framework_TestCase {

    const TRANSER_EXCEPTION_MESSAGE = 'TRANSER_EXCEPTION_MESSAGE';
    const TRANSER_EXCEPTION_CODE = 1;
    const RESPONSE_MODEL_ERROR_MESSAGE = 'RESPONSE_MODEL_ERROR_MESSAGE';
    const RESPONSE_MODEL_ERROR_CODE = 2;
    const DESERIALISATION_EXCEPTION_MESSAGE = 'DESERIALISATION_EXCEPTION_MESSAGE';
    const DESERIALISATION_EXCEPTION_CODE = 3;

    private $errorModel;
    private $errorModelWithErrors;

    public function setUp() {
        $this->errorModel = static::constructNewErrorModel();
        $this->errorModelWithErrors = static::getErrorModelWithErrors();
    }

    public function test__construct() {
        $this->assertEquals(static::getIntegrator(), static::constructNewErrorModel()->getApiIntegrator());
    }

    public function testHasTransferError() {
        $this->assertFalse($this->errorModel->hasTransferError());
        $this->assertTrue($this->errorModelWithErrors->hasTransferError());
    }

    public function testGetTransferExceptionMessage() {
        $this->assertNull($this->errorModel->getTransferExceptionMessage());
        $this->assertSame(static::TRANSER_EXCEPTION_MESSAGE, $this->errorModelWithErrors->getTransferExceptionMessage());
    }

    public function testGetTransferExceptionCode() {
        $this->assertNull($this->errorModel->getTransferExceptionCode());
        $this->assertSame(static::TRANSER_EXCEPTION_CODE, $this->errorModelWithErrors->getTransferExceptionCode());
    }

    public function testHasResponseError() {
        $this->assertFalse($this->errorModel->hasResponseError());

        $errorModel = new ApiIntegratorErrorModel();
        $errorModel->setResponseModelErrorMessage(true);
        $errorModel->setResponseModelErrorCode(true);
        $this->assertTrue($errorModel->hasResponseError());

        $errorModel->setResponseModelErrorMessage(null);
        $this->assertTrue($errorModel->hasResponseError());

        $errorModel->setResponseModelErrorCode(null);
        $this->assertFalse($errorModel->hasResponseError());
    }

    public function testGetResponseModelErrorMessage() {
        $this->assertNull($this->errorModel->getResponseModelErrorMessage());
        $this->assertSame(static::RESPONSE_MODEL_ERROR_MESSAGE, $this->errorModelWithErrors->getResponseModelErrorMessage());
    }

    public function testGetResponseModelErrorCode() {
        $this->assertNull($this->errorModel->getResponseModelErrorCode());
        $this->assertSame(static::RESPONSE_MODEL_ERROR_CODE, $this->errorModelWithErrors->getResponseModelErrorCode());
    }

    public function testHasDeserialiseError() {
        $this->assertFalse($this->errorModel->hasDeserialiseError());
        $this->assertTrue($this->errorModelWithErrors->hasDeserialiseError());
    }

    public function testGetDeserialiseExceptionMessage() {
        $this->assertNull($this->errorModel->getDeserialiseExceptionMessage());
        $this->assertSame(static::DESERIALISATION_EXCEPTION_MESSAGE, $this->errorModelWithErrors->getDeserialiseExceptionMessage());
    }

    public function testGetDeserialiseExceptionCode() {
        $this->assertNull($this->errorModel->getDeserialiseExceptionCode());
        $this->assertSame(static::DESERIALISATION_EXCEPTION_CODE, $this->errorModelWithErrors->getDeserialiseExceptionCode());
    }

    public function testHasErrors() {
        $this->assertFalse($this->errorModel->hasErrors());
        $this->assertTrue($this->errorModelWithErrors->hasErrors());

        $this->errorModelWithErrors->setTransferExceptionMessage(null);
        $this->errorModelWithErrors->getApiIntegrator()->setTransferException(null);
        $this->assertTrue($this->errorModelWithErrors->hasErrors());

        $this->errorModelWithErrors->getApiIntegrator()->getResponseModel()->setError(null);
        $this->errorModelWithErrors->setResponseModelErrorMessage(null);
        $this->errorModelWithErrors->getApiIntegrator()->getResponseModel()->setErrorCode(null);
        $this->errorModelWithErrors->setResponseModelErrorCode(null);
        $this->assertTrue($this->errorModelWithErrors->hasErrors());

        $this->errorModelWithErrors->getApiIntegrator()->getResponseModel()->setDeserialiseException(null);
        $this->errorModelWithErrors->setDeserialiseExceptionMessage(null);
        $this->assertFalse($this->errorModelWithErrors->hasErrors());
    }

    public static function getErrorModelAsArray() {
        return  [
            'transferExceptionMessage' => null,
            'transferExceptionCode' => null,
            'responseModelErrorMessage' => null,
            'responseModelErrorCode' => null,
            'deserialiseExceptionMessage' => null,
            'deserialiseExceptionCode' => null,
        ];
    }

    public static function getIntegrator() {
        $integrator = Integrator::constructNewIntegrator();
        $integrator->setResponseModel(new ResponseModel());
        return $integrator;
    }

    public static function constructNewErrorModel() {
        return new ApiIntegratorErrorModel(static::getIntegrator());
    }

    private static function getTransferException() {
        return new TransferException(static::TRANSER_EXCEPTION_MESSAGE, static::TRANSER_EXCEPTION_CODE);
    }

    private static function getNewDeserialiseException() {
        return new Exception(static::DESERIALISATION_EXCEPTION_MESSAGE, static::DESERIALISATION_EXCEPTION_CODE);
    }

    private static function getResponseModelWithErrors() {
        $responseModel = new ResponseModel();
        $responseModel->setError(static::RESPONSE_MODEL_ERROR_MESSAGE);
        $responseModel->setErrorCode(static::RESPONSE_MODEL_ERROR_CODE);
        $responseModel->setDeserialiseException(static::getNewDeserialiseException());
        return $responseModel;
    }

    private static function getIntegratorWithErrors() {
        $integrator = Integrator::constructNewIntegrator();
        $integrator->setResponseModel(static::getResponseModelWithErrors());
        $integrator->setTransferException(static::getTransferException());
        return $integrator;
    }

    public static function getErrorModelWithErrors() {
        return new ApiIntegratorErrorModel(static::getIntegratorWithErrors());
    }
}
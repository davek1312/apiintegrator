<?php

namespace Davek1312\ApiIntegrator\Tests\Mock;

use Davek1312\ApiIntegrator\ApiIntegrator;
use Davek1312\ApiIntegrator\Tests\Mock\Models\RequestModel;
use Davek1312\ApiIntegrator\Tests\Mock\Models\ResponseModel;

class Integrator extends ApiIntegrator {

    const REQUEST_TIME_IN_SECONDS = 5.0;
    const HTTP_STATUS_CODE = 200;

    /**
     * @var string
     */
    public static $responseModelClass = ResponseModel::class;

    /**
     * @return Integrator
     */
    public static function constructNewIntegrator() {
        return new static(static::getNewRequestModel());
    }

    public static function getNewRequestModel() {
        return RequestModel::constructNewRequestModel();
    }

    public static function getNewResponseModel() {
        return new ResponseModel();
    }
}
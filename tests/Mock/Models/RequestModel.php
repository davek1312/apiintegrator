<?php

namespace Davek1312\ApiIntegrator\Tests\Mock\Models;

use Davek1312\ApiIntegrator\Models\ApiIntegratorRequestModel;

class RequestModel {

    const URL = 'http://jsonplaceholder.typicode.com/comments/1';
    const CONNECTION_METHOD = 'get';
    const CONNECTION_TIMEOUT = 5.1;
    const REQUEST_TIMEOUT = 6.1;
    const ADDITIONAL_REQUEST_OPTIONS = ['debug' => false, 'verify' => false];
    const REQUEST_MODEL_ARRAY = [
        'url' => RequestModel::URL,
        'connectionMethod' => RequestModel::CONNECTION_METHOD,
        'connectTimeout' => RequestModel::CONNECTION_TIMEOUT,
        'requestTimeout' => RequestModel::REQUEST_TIMEOUT,
        'additionalRequestOptions' => RequestModel::ADDITIONAL_REQUEST_OPTIONS,
    ];

    /**
     * @return ApiIntegratorRequestModel
     */
    public static function constructNewRequestModel() {
        return new ApiIntegratorRequestModel(static::URL, static::CONNECTION_METHOD, static::CONNECTION_TIMEOUT, static::REQUEST_TIMEOUT, static::ADDITIONAL_REQUEST_OPTIONS);
    }
}
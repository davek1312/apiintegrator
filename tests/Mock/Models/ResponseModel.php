<?php

namespace Davek1312\ApiIntegrator\Tests\Mock\Models;

use Davek1312\ApiIntegrator\Models\ApiIntegratorResponseModel;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\SerializedName;

class ResponseModel extends ApiIntegratorResponseModel {

    const RESPONSE_JSON =
    '{
      "postId": 1,
      "id": 1,
      "name": "id labore ex et quam laborum",
      "email": "Eliseo@gardner.biz",
      "body": "laudantium enim quasi est quidem magnam voluptate ipsam eos\ntempora quo necessitatibus\ndolor quam autem quasi\nreiciendis et nam sapiente accusantium"
    }';

    /**
     * @Type("integer")
     * @SerializedName("postId")
     */
    private $postId;
    /**
     * @Type("integer")
     */
    private $id;
    /**
     * @Type("string")
     */
    private $name;
    /**
     * @Type("string")
     */
    private $email;
    /**
     * @Type("string")
     */
    private $body;
    /**
     * @Exclude()
     */
    private $error;
    /**
     * @Exclude()
     */
    private $errorCode;

    public static function getResponseDataType() {
        return 'json';
    }

    public function getPostId() {
        return $this->postId;
    }

    public function setPostId($postId) {
        $this->postId = $postId;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getBody() {
        return $this->body;
    }

    public function setBody($body) {
        $this->body = $body;
    }

    public function getError() {
        return $this->error;
    }

    public function setError($error) {
        $this->error = $error;
    }

    public function getErrorCode() {
        return $this->errorCode;
    }

    public function setErrorCode($errorCode) {
        $this->errorCode = $errorCode;
    }

    public function getResponseErrorMessage() {
        return $this->error;
    }

    public function getResponseErrorCode() {
        return $this->errorCode;
    }

    public static function getResponseModelFromJson() {
        return static::deserialiseJson(static::RESPONSE_JSON);
    }
}
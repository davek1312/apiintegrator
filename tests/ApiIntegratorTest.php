<?php

namespace Davek1312\ApiIntegrator\Tests;

use Davek1312\ApiIntegrator\Models\ApiIntegratorErrorModel;
use Davek1312\ApiIntegrator\Tests\Mock\Integrator;
use Davek1312\ApiIntegrator\Tests\Mock\Models\ResponseModel;
use GuzzleHttp\Exception\TransferException;

class ApiIntegratorTest extends \PHPUnit_Framework_TestCase {

    public function test__construct() {
        $integrator = Integrator::constructNewIntegrator();
        $this->assertEquals(Integrator::getNewRequestModel(), $integrator->getRequestModel());
    }

    public function testGenerateResponseModel() {
        $integrator = Integrator::constructNewIntegrator();
        $responseModel = $integrator->generateResponseModel();
        $this->assertNotNull($responseModel);
        $this->assertEquals($responseModel, $integrator->getResponseModel());
        $this->assertInstanceOf(ResponseModel::class, $responseModel);
    }

     public function testGenerateNewResponseModel() {
         $integrator = Integrator::constructNewIntegrator();
         $responseModel = $integrator->generateNewResponseModel(2);
         $this->assertValidResponseModel($responseModel, $integrator);
         $this->assertEquals(1, $integrator->getCountRequestAttempts());
    }

    private function assertValidResponseModel($responseModel, $integrator) {
        $this->assertNotNull($responseModel);
        $this->assertEquals($responseModel, $integrator->getResponseModel());
        $this->assertInstanceOf(ResponseModel::class, $responseModel);
        $this->assertSame(1, $responseModel->getPostId());
        $this->assertSame(1, $responseModel->getId());
        $this->assertSame('id labore ex et quam laborum', $responseModel->getName());
        $this->assertSame('Eliseo@gardner.biz', $responseModel->getEmail());
        $this->assertSame("laudantium enim quasi est quidem magnam voluptate ipsam eos\ntempora quo necessitatibus\ndolor quam autem quasi\nreiciendis et nam sapiente accusantium", $responseModel->getBody());
        $this->assertValidHttpResponse($integrator);
    }

    public function testGenerateNewHttpResponse() {
        $validIntegrator = Integrator::constructNewIntegrator();
        $validIntegrator->generateNewHttpResponse();
        $this->assertValidHttpResponse($validIntegrator);

        $invalidIntegrator = Integrator::constructNewIntegrator();
        $invalidIntegrator->getRequestModel()->setUrl('http://www.rgeeghinvalid_url.com');
        $invalidIntegrator->generateNewHttpResponse();
        $this->assertInValidHttpResponse($invalidIntegrator);
    }

    private function assertValidHttpResponse($integrator) {
        $this->assertNull($integrator->getTransferException());
        $this->assertValidProcessTransferStats($integrator);
        $this->assertValidProcessHttpResponse($integrator);
    }

    private function assertValidProcessTransferStats($integrator) {
        $this->assertGreaterThan(0, $integrator->getRequestTimeInSeconds());
        $this->assertSame(200, $integrator->getHttpStatusCode());
    }

    private function assertValidProcessHttpResponse($integrator) {
        $response = $integrator->getHttpResponse();
        $this->assertNotNull($response);
        $this->assertInstanceOf('GuzzleHttp\Psr7\Response', $response);
        //Removing white space as they just don't match on line endings
        $this->assertSame(preg_replace('/\s+/', '', ResponseModel::RESPONSE_JSON), preg_replace('/\s+/', '', $integrator->getResponseBodyContents()));
    }

    private function assertInValidHttpResponse($integrator) {
        $transferException = $integrator->getTransferException();
        $this->assertNotNull($transferException);
        $this->assertInstanceOf('GuzzleHttp\Exception\TransferException', $transferException);
        $this->assertInValidProcessTransferStats($integrator);
        $this->assertInValidProcessHttpResponse($integrator);
    }

    private function assertInValidProcessTransferStats($integrator) {
        $this->assertNull($integrator->getHttpStatusCode());
        $this->assertNull($integrator->getHttpStatusCode());
    }

    private function assertInValidProcessHttpResponse($integrator) {
        $this->assertNull($integrator->getResponseBodyContents());
        $response = $integrator->getHttpResponse();
        $this->assertNull($response);
    }

    public function testHasTransferError() {
        $integrator = Integrator::constructNewIntegrator();
        $this->assertFalse($integrator->hasTransferError());

        $integrator->setTransferException(new TransferException());
        $this->assertTrue($integrator->hasTransferError());
    }

    public function testGetErrorModel() {
        $integrator = Integrator::constructNewIntegrator();
        $errorModel = $integrator->getErrorModel();
        $this->assertNotNull($errorModel);
        $this->assertInstanceOf(ApiIntegratorErrorModel::class, $errorModel);
    }

    public function testHasErrors() {
        $integrator = Integrator::constructNewIntegrator();
        $this->assertFalse($integrator->hasErrors());

        $integrator->setTransferException(new TransferException('Test Error Message'));
        $this->assertTrue($integrator->hasErrors());
    }
}